"""
QUESTION - https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem
"""

def array_left_rotation(a, n, k):
   
   
    new_array = (a[k:n+1] + a[0:k])
    return (new_array)
    

n , k = map(int, input().strip().split(' '))
a = list(map(int, input().strip().split(' ')))
answer = array_left_rotation(a, n, k);
print(*answer, sep=' ')